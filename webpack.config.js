const WebpackDeepScopeAnalysisPlugin = require('webpack-deep-scope-plugin').default;

module.exports = {
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', {
          loader: 'css-loader?modules&localIdentName=[path][name]__[local]--[hash:base64:5]'
        }],
      },
    ],
  },
  plugins: [
    new WebpackDeepScopeAnalysisPlugin(),
  ],
}